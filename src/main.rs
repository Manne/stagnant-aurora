use bevy::prelude::*;

fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .add_systems(Startup, setup)
        .add_systems(Update, (set_global_mouse_position, cursor_hover))
        .add_systems(Update, bevy::window::close_on_esc)
        .add_systems(
            Update,
            (
                set_global_mouse_position,
                cursor_hover,
                change_color_on_hover,
            ),
        )
        .run();
}

#[derive(Default, Resource, Deref, DerefMut)]
struct CameraPos(Vec2);

#[derive(Component)]
struct WorldCamera;

#[derive(Component, Deref, DerefMut)]
struct Size(Vec2);

#[derive(Component, Deref, DerefMut)]
struct Hovered(bool);

fn setup(mut commands: Commands) {
    commands.init_resource::<CameraPos>();
    commands.spawn((Camera2dBundle::default(), WorldCamera));
    let size = Vec2::new(50.0, 100.0);
    commands.spawn((
        SpriteBundle {
            sprite: Sprite {
                color: Color::rgb(0.25, 0.25, 0.75),
                custom_size: Some(size),
                ..default()
            },
            transform: Transform::from_translation(Vec3::new(-50., 0., 1.)),
            ..default()
        },
        Size(size),
        Hovered(false),
    ));
}

fn change_color_on_hover(mut hovered_sprite: Query<(&mut Sprite, &Hovered), Changed<Hovered>>) {
    for (mut sprite, hovered) in hovered_sprite.iter_mut() {
        if **hovered {
            sprite.color = Color::rgb(0., 1., 0.);
        } else {
            sprite.color = Color::rgb(0., 0., 1.);
        }
    }
}

fn cursor_hover(
    camera_pos: Res<CameraPos>,
    mut hoverables: Query<(&GlobalTransform, &Size, &mut Hovered)>,
) {
    let mut should_hover = false;
    for (position, size, mut hovered) in hoverables.iter_mut() {
        let translation = position.translation();
        if aabb(&camera_pos, &translation, size) {
            should_hover = true;
        }
        if should_hover != **hovered {
            **hovered = should_hover
        }
    }
}

fn set_global_mouse_position(
    mut pos: ResMut<CameraPos>,
    camera: Query<(&Camera, &GlobalTransform), With<WorldCamera>>,
    mut cursor: EventReader<CursorMoved>,
) {
    for cursor in cursor.iter() {
        let (camera, camera_transform) = camera.single();
        if let Some(world_position) = camera.viewport_to_world_2d(camera_transform, cursor.position)
        {
            **pos = world_position;
        }
    }
}

fn aabb(a_loc: &Vec2, b_loc: &Vec3, size: &Vec2) -> bool {
    a_loc.x > b_loc.x - size.x / 2.0
        && a_loc.x < b_loc.x + size.x / 2.0
        && a_loc.y > b_loc.y - size.y / 2.0
        && a_loc.y < b_loc.y + size.y / 2.0
}
